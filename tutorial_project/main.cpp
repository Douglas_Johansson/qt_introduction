#include "mynotepad.h"
#include <QCommandLineParser>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::applicationName());
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("file", "The file to open.");
    parser.process(a);

    QCoreApplication::setOrganizationName("Experis");
    QCoreApplication::setApplicationName("Notepad");
    QCoreApplication::setApplicationVersion("1.0");

    MyNotepad w;

    if (!parser.positionalArguments().isEmpty())
        w.loadFile(parser.positionalArguments().first());
    w.show();
    return a.exec();
}
