#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    point = 0;
    ui->progressBar->setRange(0, 10);
    ui->progressBar->setValue(0);
    timeLine = new QTimeLine(10000, this);
    timeLine->setFrameRange(0, 10);
    connect(timeLine, &QTimeLine::frameChanged, ui->progressBar, &QProgressBar::setValue);
    connect(timeLine, &QTimeLine::finished, this, [this](){on_answer_played(-1);});
    ui->stackedWidget->setCurrentIndex(0);
    currentQuestion = 0;
    questions.push_back("What year was queen Elizabeth born?");
    std::vector<QString> answersForFirst;
    answersForFirst.push_back("1926");
    answersForFirst.push_back("1922");
    answersForFirst.push_back("1918");
    answersForFirst.push_back("1929");
    vectorOfAnswer.push_back(answersForFirst);
    correctAnswer.push_back(0);

    std::vector<QString> answersForSecond;
    questions.push_back("What year did Queen Elizabeth die?");
    answersForSecond.push_back("2010");
    answersForSecond.push_back("2019");
    answersForSecond.push_back("2025");
    answersForSecond.push_back("2022");
    vectorOfAnswer.push_back(answersForSecond);
    correctAnswer.push_back(3);

    std::vector<QString> answersForThird;
    questions.push_back("Who won the Swedish election?");
    answersForThird.push_back("S");
    answersForThird.push_back("SD");
    answersForThird.push_back("C");
    answersForThird.push_back("MP");
    vectorOfAnswer.push_back(answersForThird);
    correctAnswer.push_back(0);

    connect(ui->alternativeOne,&QPushButton::released, this, [this](){emit checkAnswer(0);});
    connect(ui->alternativeTwo,&QPushButton::released, this, [this](){emit checkAnswer(1);});
    connect(ui->alternativeThree,&QPushButton::released, this, [this](){emit checkAnswer(2);});
    connect(ui->alternativeFour,&QPushButton::released, this, [this](){emit checkAnswer(3);});
    connect(this, &MainWindow::checkAnswer, this, &MainWindow::on_answer_played);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_playButton_clicked()
{

    try {
    name = ui->enterName->text();
    ui->stackedWidget->setCurrentIndex(1);
    ui->question->setText(questions[currentQuestion]);
    ui->alternativeOne->setText(vectorOfAnswer[currentQuestion][0]);
    ui->alternativeTwo->setText(vectorOfAnswer[currentQuestion][1]);
    ui->alternativeThree->setText(vectorOfAnswer[currentQuestion][2]);
    ui->alternativeFour->setText(vectorOfAnswer[currentQuestion][3]);
    timeLine->start();
    }
    catch(...) {
        qInfo() << "Something went wrong \n";
    }
}


void MainWindow::on_answer_played(int answerPlayed)
{
    if(answerPlayed == correctAnswer[currentQuestion])
    {
        point++;
    }
    timeLine->stop();
    ui->progressBar->setValue(0);
    currentQuestion++;
    if(currentQuestion == 3){
        ui->label->setText(name + " got "+ QString::number(point) + " points");
        ui->stackedWidget->setCurrentIndex(2);

    } else {
    on_playButton_clicked();
    }
}

