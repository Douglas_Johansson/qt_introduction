#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimeLine>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    std::vector<QString> questions;
    std::vector<std::vector<QString>> vectorOfAnswer;
    std::vector<int> correctAnswer;


signals:
    void checkAnswer(int answerPressed);


public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int currentQuestion;
    int point;
    QString name;
    QTimeLine *timeLine;

private slots:
    void on_playButton_clicked();    
    void on_answer_played(int answerPlayed);

private:
    Ui::MainWindow *ui;

};
#endif // MAINWINDOW_H
