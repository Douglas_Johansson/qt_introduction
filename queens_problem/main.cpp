#include <QCoreApplication>
#include <iostream>
#include <vector>
#include <QTextStream>
#include <algorithm>
#include <QDebug>

bool isValid(const std::vector<int> &v)
{

    int value = v.back(); // Most recent queen

    // If the same value already exists the row is already occupied, return false.
    for (std::size_t i = 0; i < v.size() - 1; i++)
    {
        if (v[i] == value)
        {
            return false;
        }
    }

    // Check diagonally in both directions
    int offset = 1;

    for (int i = v.size() - 2; i >= 0; --i)

    {
        if (value == v[i] + offset)
        {
            return false;
        }

        if (value == v[i] - offset)

        {
            return false;
        }
        ++offset;
    }

    return true;
}

void drawBoard(std::vector<int> &v, int totalToPlace, int amountOfSolutions)
{

    for(int i = 0; i < totalToPlace; i++)
    {
        for (int j = 0; j < totalToPlace; j++)
        {
            if(v[i] == j)
            {
                QTextStream(stdout) << "Q";
            }
            else {QTextStream(stdout) << "*";}
        }

        QTextStream(stdout) << "\n";
    }



}

void nQueens(std::vector<int> &v, int leftToPlace, int totalToPlace, int &amountOfSolutions)
{
    if (leftToPlace == 0)  // When the algorithms has managed to put all queens on the board and abiding the rules
    {
        drawBoard(v, totalToPlace,amountOfSolutions);
        amountOfSolutions++;
        QTextStream(stdout) << "________________Solution number: " <<amountOfSolutions<< "__________________\n";
        return;
    }
    // To avoid pop/push in loop temporary add a 0 at the back
    v.push_back(0);

    for (int i = 0; i < totalToPlace; i++)
    {
        // Changes value of the temp 0 to location we want to try
        v.back() = i;

        // check if Queen is at a valid location. The location is valid if there are no other queen on the same row
        if (isValid(v))
        {
            nQueens(v, leftToPlace - 1, totalToPlace, amountOfSolutions);
        }
    }
    v.pop_back();
}

void nQueens(int n)
{
    std::vector<int> v;
    int amountOfSolutions = 0;
    v.reserve(n);
    nQueens(v, n, n, amountOfSolutions);
    qInfo() << "Amount of solutions: " << amountOfSolutions;
}





int main(int argc, char *argv[])
{

    QCoreApplication a(argc, argv);
    qInfo() << "Board size?";
    QTextStream qin(stdin);
    int n;
    qin >> n;
    if(n>13){
     qInfo() << "I don't have time for that";
            return 0;
        }
        nQueens(n);



    return a.exec();
}
