#include <QCoreApplication>
#include <QDebug>
#include <map>
#include <limits.h>


// Function to calculate fibonacci number by saving last two numbers
unsigned int fib(int n)
{
    int a = 0, b = 1, c, i;
    if( n == 0){return a;}

    for(i = 2; i <= n; i++)
    {
       c = a + b;
       a = b;
       b = c;

    }
    return b;
}





int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString instructions = "Which fibonacci number would you like to know?";
    int number = 1;

    while(true){
    qInfo() << instructions;
    QTextStream qin(stdin);
    qin >> number;
    Q_ASSERT(number != 0); // End loop if input is 0 or not a number.
    Q_ASSERT(fib(number) < INT_MAX); // Break if number is bigger than an int can store
    qInfo() << fib(number);

}

}
